package ds.bookstore.test;

import ds.bookstore.Book;
import ds.bookstore.BookStore;
import junit.framework.TestCase;

import java.io.File;
import java.io.IOException;

public class BookStoreTest extends TestCase {
    public void testBookPlacement() {
        BookStore store = new BookStore();

        store.putBook(new Book("Duna"));
        assertEquals(1, store.size());

        store.putBook(new Book("Oliver Twist"));
        assertEquals(2, store.size());
    }

    public void testBookRetrieval() {
        BookStore store = new BookStore();
        String[] titles = titles();
        Book[] books = books(titles);

        store.putBook(books[0]);
        store.putBook(books[1]);

        Book firstBook = store.getBook(titles[0]);
        Book secondBook = store.getBook(titles[1]);
        assertEquals(titles[0], firstBook.getTitle());
        assertEquals(titles[1], secondBook.getTitle());
    }

    public void testSerialization() throws IOException, ClassNotFoundException {
        BookStore bookStore = defaultBookStore();
        String fileName = "default.bookstore";
        BookStore.save(bookStore, fileName);

        File file = new File(fileName);
        assertTrue(file.isFile());

        BookStore loaded = BookStore.loadFromFile(fileName);
        assertEquals(bookStore.size(), loaded.size());

        String[] titles = titles();
        Book fromFile = loaded.getBook(titles[1]);
        Book fromDefault = bookStore.getBook(titles[1]);
        assertEquals(fromFile.getTitle(), fromDefault.getTitle());
    }

    private String[] titles() {
        return new String[]{ "Duna", "Oliver Twist" };
    }

    private Book[] books(String[] titles) {
        Book[] books = new Book[titles.length];
        for (int index = 0; index < titles.length; index++) {
            books[index] = new Book(titles[index]);
        }
        return books;
    }

    private Book[] books() {
        return books(titles());
    }

    private BookStore defaultBookStore() {
        BookStore bookStore = new BookStore();
        for (Book book : books()) {
            bookStore.putBook(book);
        }
        return bookStore;
    }
}