package ds.bookstore.server;

import jdk.internal.util.xml.impl.Input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.String;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

/**
 * Created by fellipe on 12/03/15.
 */
public class ServerMain {
    private static final int PORT = 9999;

    public static void main(String[] args) throws IOException {
        // Criar um servidor de sockets na porta PORT.
        ServerSocket server = new ServerSocket(PORT);
        System.out.printf("Listening to port %d\n", PORT);

        // Aceitar uma nova conexao.
        // O programa para nessa linha ate um novo
        // socket se conectar.
        Socket connection = server.accept();
        System.out.printf("New connection: %s\n", connection.getInetAddress());

        // Criar um leitor para o fluxo de dados da conexao.
        // Um socket se trata de um canal de comunicacao
        // entre duas aplicacoes entao eu preciso ser capaz
        // de ler os dados desse canal.
        InputStream inputStream = connection.getInputStream();
        InputStreamReader streamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(streamReader);
        String message = bufferedReader.readLine();
        System.out.printf("New message from %s: %s\n",
                connection.getInetAddress(), message);

        // Apos o uso, fechar a conexao.
        // Portanto, esse servidor que criei trabalha
        // com apenas uma conexao. Depois disso, o programa
        // termina.
        connection.close();
        server.close();
    }
}
