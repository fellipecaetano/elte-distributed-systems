package ds.bookstore;

import java.io.*;
import java.util.HashMap;

/**
 * Created by fellipe on 11/03/15.
 */
public class BookStore implements Serializable {
    private HashMap<String, Book> storage;

    public BookStore() {
        storage = new HashMap<String, Book>();
    }

    public void putBook(Book book) {
        storage.put(book.getTitle(), book);
    }

    public int size() {
        return storage.size();
    }

    public Book getBook(String title) {
        return storage.get(title);
    }

    public static void save(BookStore bookStore, String fileName) throws IOException {
        FileOutputStream fileOutput = new FileOutputStream(fileName);
        ObjectOutputStream objectOutput = new ObjectOutputStream(fileOutput);
        objectOutput.writeObject(bookStore);
        objectOutput.close();
    }

    public static BookStore loadFromFile(String fileName) throws IOException, ClassNotFoundException {
        FileInputStream fileInput = new FileInputStream(fileName);
        ObjectInputStream objectInput = new ObjectInputStream(fileInput);
        Object object = objectInput.readObject();
        objectInput.close();
        return (BookStore) object;
    }
}
