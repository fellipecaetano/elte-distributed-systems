package ds.bookstore.client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by fellipe on 12/03/15.
 */
public class ClientMain {
    private static final String HOST = "localhost";
    private static final int PORT = 9999;

    public static void main(String[] args) throws IOException {
        // Criar uma nova conexao com um servidor
        // no endereco HOST:PORT
        Socket connection = new Socket(HOST, PORT);

        // Criar um escritor para o fluxo de dados
        // dessa conexao.
        OutputStream stream = connection.getOutputStream();
        PrintWriter writer = new PrintWriter(stream, true);

        // Escrever um mensagem para o fluxo de dados.
        // Se a outra ponta da conexao desejar,
        // ela pode ler essa mensagem.
        writer.println("Hello, world!");
        writer.close();

        // Apos o uso, fechar a conexao.
        connection.close();
    }
}
