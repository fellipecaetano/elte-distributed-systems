package ds.bookstore;

import java.io.Serializable;

/**
 * Created by fellipe on 11/03/15.
 */
public class Book implements Serializable {
    private String title;

    public Book(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
